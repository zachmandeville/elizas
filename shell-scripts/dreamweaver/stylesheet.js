module.exports = view

function view () {
  return `
  body{
    background: peachpuff;
    font-family: cursive;
    font-size: 20px;
    color: indigo;
  }

  .frame {
    position: relative;
    max-width: 80%;
    margin: auto;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
  }
  li {
    list-style-type: none;
    padding: 0;
  }
  img {
    width: 100%;
  }
  `
}

