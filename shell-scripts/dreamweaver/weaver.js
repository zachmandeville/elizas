#!/usr/bin/env node
var fs = require('fs')
var html = require('nanohtml')

var stylesheet = require('./stylesheet')

var args = process.argv.slice(2)

var picsDirectory = args[0]
var title = args[1]

fs.readdir(picsDirectory, 'utf-8', renderToHtml)

function renderToHtml (err, files) {
  if (err) throw err
  fs.writeFile('index.html', toHtml(files), (err) => {
    if (err) throw err
    console.log('index is a success')
  })
  fs.writeFile('stylesheet.css', stylesheet(), (err) => {
    if (err) throw err
    console.log('style is a success')
  })
}

function toHtml (files) {
  return html`
  <!doctype HTML>
    <html>
    <head>
    <title>${title}</title>
    <link rel='stylesheet' href='stylesheet.css' />
    </head>
    <body>
    <h1>${title}</h1>
  <ul>
    ${files.map(file => makeList(file))}
  </ul>
    </body>
    </html>
    `

  function makeList (file) {
    return html`
      <li>
      <div class='frame'>
      <img src=${file} alt=${file} />
      </div>
      </li>
    `
  }
}
