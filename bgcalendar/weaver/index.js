const dayjs = require('dayjs')
const icalDateParser = require('ical-date-parser')
const sortBy = require('lodash/sortBy')

const eventSnippet = require('./components/event-snippet')
const fullPage = require('./components/full-page')

function weave (events, style) {
  var allUpcomingEvents = gatherUpcomingEvents(events)
  var nextEvent = allUpcomingEvents[0]
  if (style === 'full') return fullPage(allUpcomingEvents)
  if (style === 'single') return eventSnippet(nextEvent)
  else console.log('style option not recognized')
}

function gatherUpcomingEvents (events) {
  var formattedEvents = []
  events.forEach(event => {
      formattedEvents.push({
        SUMMARY: event.SUMMARY,
        DESCRIPTION: event.DESCRIPTION,
        LOCATION: event.LOCATION,
        DATE: dateAsObject(event.DTSTART),
        RAW: event
      })
    })
  return sortBy(formattedEvents, (event) => event.timeStamp)
}

function dateAsObject (DTSTART) {
  var dateAndTimes = dayjs(icalDateParser(DTSTART))

  var formattedTime = {
    year: dateAndTimes.$y,
    month: {number: dateAndTimes.$M + 1, name: dayjs(dateAndTimes).format('MMMM')},
    day: dateAndTimes.$D,
    dayOfWeek: dateAndTimes.format('dddd'),
    time: dateAndTimes.format('HH:mm'),
    date: dateAndTimes.format('YYYY-MM-DD'),
    timeStamp: dateAndTimes.unix()
  }

  return formattedTime
}

module.exports = { weave }
