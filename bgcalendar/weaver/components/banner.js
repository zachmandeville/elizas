const html = require('nanohtml')

     module.exports = () => {
       return html`
           <header id='banner'>
            <pre> 
 ######                                                              #####                          #######                                   
#     #  ####    ##   #####  #####   ####    ##   #    # ######    #     # #      #    # #####     #       #    # ###### #    # #####  ####  
#     # #    #  #  #  #    # #    # #    #  #  #  ##  ## #         #       #      #    # #    #    #       #    # #      ##   #   #   #      
######  #    # #    # #    # #    # #      #    # # ## # #####     #       #      #    # #####     #####   #    # #####  # #  #   #    ####  
#     # #    # ###### #####  #    # #  ### ###### #    # #         #       #      #    # #    #    #       #    # #      #  # #   #        # 
#     # #    # #    # #   #  #    # #    # #    # #    # #         #     # #      #    # #    #    #        #  #  #      #   ##   #   #    # 
######   ####  #    # #    # #####   ####  #    # #    # ######     #####  ######  ####  #####     #######   ##   ###### #    #   #    ####  
             </pre>
           </header>
       `
     }
