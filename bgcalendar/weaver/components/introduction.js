const html = require('nanohtml')

module.exports = () => {
 return html`
<section id="introduction">
  <p>This page holds events I am doing or attending, or that I think are cool.  I don't always get to attend everything I think is cool (far from it! Infinite sensations within a finite life and all that), so this is not an authorative calendar for where I'ma be at any time.  But if you are wanting to know, 'oh, what's Zach potentially up to right now." then check here!  And if you're thinking, "What's zach's aesthetic as defined by temporal events." Then check here too, my friend!
  </p>
</section>
`
}
