const html = require('nanohtml')

module.exports = (event) => {
  var eventDescription = () => {
    if (event.DESCRIPTION) return event.DESCRIPTION.replace(/\\/g, '')
    else return ''
  }

  return html`
  <h2>Our Next Event</h2>
  <div class="h-event vevent">
    <h3><span class="p-name p-summary summary">${event.SUMMARY}</span></h3>
    <p class='event-date'>
      <abbr title=${event.DATE.date} class="dt-start dtstart">
        ${event.DATE.dayOfWeek}, ${event.DATE.month.name} ${event.DATE.day}th at ${event.DATE.time}
      </abbr>
    </p>
    <p class='p-location location'>
      <strong class='location-title'>Location: </strong> ${event.LOCATION}
    </p>
  <div class="p-description description">
    <p>${eventDescription}</p>
    <a href="${event.RAW.UID}.ics" title="ics file for ${event.SUMMARY}">
      Download to your own calendar!
    </a>
  </div>
  <strong>Check out all upcoming events <a href='calendar/index.html' title='upcoming events page'>Here</a></strong>
</div>
  `
}
