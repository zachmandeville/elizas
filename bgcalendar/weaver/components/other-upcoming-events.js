const html = require('nanohtml')

const listEvents = require('./list-events.js')

module.exports = (events) => {
  if (events.length === 0) return null
  return html`
  <div id='this-week'>
    <h2>Upcoming Events!</h2>
    <ul>${listEvents(events)} </ul>
  </div>
    `
  }
