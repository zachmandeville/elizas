const html = require('nanohtml')
const drop = require('lodash/drop')

const head = require('./head')
const breadcrumb = require('./breadcrumb')
const banner = require('./banner')
const introduction = require('./introduction')
const nextEvent = require('./next-event')
const otherUpcomingEvents = require('./other-upcoming-events')

module.exports = (upcomingEvents) => {
  return html `
<!doctype HTML>
<html>
  ${head()}
  <body>
    ${breadcrumb()}
    ${banner()}
    ${introduction()}
    <div id='events'>
      ${nextEvent(upcomingEvents[0])}
      ${otherUpcomingEvents(drop(upcomingEvents,1))}
    </div>
  </body>
</html>
`
}
