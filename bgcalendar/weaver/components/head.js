const html = require('nanohtml')
  module.exports = () => {
    return html`
    <head>
      <title>coolguy.website | Calendar</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content = "width=device=width, initial-scale=1">
      <meta name='author' content='yr friend and mine Zach Mandeville!'>
      <meta name='description' content='The online calendar  of writer, developer, solarpunk, and friend Zach Mandeville.'> 
      <meta property="og:title" content="Coolguy.Website | Calendar' />
      <meta property="og:type" content="website" />
      <meta name='description' content='The online calendar  of writer, developer, solarpunk, and friend Zach Mandeville.'>
      <meta property="og:image" content="https://coolguy.website/images/zach-dancing-with-robot.gif" />
      <meta property="og:url" content="https://coolguy.website/" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content="Coolguy.Website | Calendar' />
      <meta name='twitter:description' content='The online calendar  of writer, developer, solarpunk, and friend
      Zach Mandeville.'>
      <meta name="twitter:image" content="https://coolguy.website/images/zach-dancing-with-robot.gif" />
      <link rel='stylesheet' type='text/css' href='./stylesheet.css' />
    </head>
    `
  }
