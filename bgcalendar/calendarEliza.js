#!/usr/bin/env node
const axios = require('axios')
const ical2json = require('ical2json')
const icalDateParser = require('ical-date-parser')
const fs = require('fs')
const forEach = require('lodash/forEach')
const mapKeys = require('lodash/mapKeys')
const startsWith = require('lodash/startswith')
const sortBy = require('lodash/sortBy')
const trimEnd = require('lodash/trimEnd')

const weaver = require('./weaver')
const calJson = require('./calendar.json')

// Our Calendar. Switch this to yr calendar if it ever changes.
var feedUrl = 'https://p.fruux.com/c/a3298186395/5ebbe7cf-a7f5-47b1-ba08-70955a9b5f3d.ics'

var icsHeader = `
BEGIN:VCALENDAR
X-ORIGINAL-URL:https://boardgameclub.solarpunk.cool
X-WR-CALNAME;CHARSET=utf-8:Wellington Baordgame Club Calendar!
VERSION:2.0
METHOD:PUBLISH
BEGIN:VEVENT
`
var icsFooter = `
END:VEVENT
END:VCALENDAR
`

axios.get(feedUrl)
  .then((icsData) => updateBoardgameCalendar(icsData))
  .catch(err => console.log(`hey, you threw an error, but that's okay.  We all do it.  But the first step is going to be to fix it.  Check out axios.get: \n ${err}`))

function updateBoardgameCalendar (icsData) {
  var calData = convertFromICS(icsData.data)
  fs.writeFile('calendar.json', calData, (err) => {
    if (err) throw err
    makeIcsFiles(calJson)
    updateCalendarPage(calJson)
  })
}

function convertFromICS (icsData) {
  rawJson = ical2json.convert(icsData)
  return JSON.stringify(rawJson, null, 2)
}

function updateCalendarPage (calJson) {
  var rawEvents = calJson.VCALENDAR[0].VEVENT
  events = formatEvents(futureDatesSorted(rawEvents))
  updatedPage = weaver.weave(events, 'full')
  nextEvent = weaver.weave(events, 'single')
  fs.writeFile('./calendar/index.html', updatedPage, 'utf-8', (err) => {
    if (err) console.log(err)
    console.log('Local BoardgameCalendar Updated!')
  })
  fs.writeFile('./calendar/nextEvent.html', nextEvent, 'utf-8', (err) => {
    if (err) console.log(err)
    console.log('nextEvent snippet made!')
  })
}

function makeIcsFiles (calJson) {
  var rawEvents = calJson.VCALENDAR[0].VEVENT
  forEach(rawEvents, (event) => {
    if (futureDate(event.DTSTART)) {
      var eventDataAsIcs = ical2json.revert(event)
      var eventIcs = icsHeader + eventDataAsIcs + icsFooter
      fs.writeFile(`./calendar/${event.UID}.ics`, eventIcs, 'utf-8', (err) => {
        if (err) console.log("yep that's an error", err)
      })
    }
  })
}

function futureDate (date) {
  var readableEventDate = icalDateParser(date)
  var eventDate = readableEventDate.setHours(0,0,0,0)
  var today = new Date().setHours(0,0,0,0)
  return eventDate > today
}

function futureDatesSorted (dates) {
  var futureDates = dates.filter(date => {
    var timestamp = date.DTSTART 
    var readableEventDate = icalDateParser(timestamp)
    var eventDate = readableEventDate.setHours(0,0,0,0)
    var today = new Date().setHours(0,0,0,0)
    return eventDate > today
  })
  return sortBy(futureDates, (date) => icalDateParser(date.DTSTART))
}

function formatEvents (events) {
  formattedEvents = []
  forEach(events, (event) => {
    var newEvent = mapKeys(event, (val, key) => {
      return makeUniform(key)
    })
    formattedEvents.push(newEvent)
  })
  return formattedEvents
}

function makeUniform (key) {
  if (startsWith(key, 'DTSTART')) {
    key = 'DTSTART'
  } else if (startsWith(key, 'DTEND')) {
    key = 'DTEND'
  }
  return key
}
