#! /usr/bin/env node
const fs = require('fs')

var homePagePath = '/Users/Nelsonian/Projects/Web/bgc/home.html'
var nextEventPath = './calendar/nextEvent.html'

var nextEvent = fs.readFileSync(nextEventPath, 'utf-8')
var homePage = fs.readFileSync(homePagePath, 'utf-8')

var updatedHomePage = homePage.replace(/<section id=\'next\-event\'>[\s\S]*?<\/section>/, '<section id=\'next\-event\'>\n' + nextEvent + '<\/section>');

fs.writeFile(homePagePath, updatedHomePage, (err, data) => {
  if (err) console.log('updateHomePage Error:', err)
  console.log('Homepage updated with next event.')
})
