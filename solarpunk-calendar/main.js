#!/usr/bin/env node
const fs = require('fs');
const fetch = require('node-fetch');
const iCal2JSON = require('ical2json');
const dayjs = require('dayjs');
const { mapKeys, mapValues } = require('lodash');
require('dotenv').config();

const CALENDAR_URL = process.env.CALENDAR_URL;
const EVENTS_PAGE_PATH = process.env.EVENTS_PAGE_PATH;
const TEMPLATE_HEAD = `
<head>
    <title>solarpunk.cool events</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content = "width=device=width, initial-scale=1">
    <meta name='author' content='Hyperlink Hall'>
    <meta name='description' content='upcoming events from ourselves and our friends'> 
    <meta property="og:title" content='solarpunk.cool calendar' />
    <meta property="og:type" content="website" />
    <meta name='description' content='upcoming events from oursels and our friends'>
    <meta property="og:image" content="https://coolguy.website/images/zach-dancing-with-robot.gif" />
    <meta property="og:url" content="https://solarpunk.cool" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content='solarpunk.cool events' />
    <meta name='twitter:description' content='upcoming events from ourselves and friends'>
    <meta name="twitter:image" content="https://coolguy.website/images/zach-dancing-with-robot.gif" />
    <link rel='stylesheet' type='text/css' href='./stylesheet.css' />
</head>
`
const TEMPLATE_HEADER = `
<header>
    <h1>solarpunk.cool events!</h1>
    <p>These are either events we put on, or that our friends are putting on and we think are cool.</p>
    <p>They are digital for the most part, so join wherever!</p>
</header>
`

const isUpcoming = (event) => event['startDate'].isAfter(dayjs());
const isRecurring = (event) => event['RRULE'];
const isSameDay = (day1, day2) => day1.format('DDMM') === day2.format('DDMM');
const isAllDay = (event) => event['startDate'].add(24, 'hour').isSame(event['endDate'])

// todo: handle longer event descriptioons (necessary when there's a good tlc event)
function Excerpt (desc) {
  // hack for getting a likely unique number in list of events.
  return [
    `<details>`,
    `<summary>`,
    desc[0],
    desc[1],
    `<button onclick=>read more</button>`,
    `</summary>`,
    ...desc.slice(2),
    `</details>`
  ].join('\n')
}

function EventArticle (event) {
  let prettyStartDate = isRecurring(event)
      ? `<p class="rrule" title="freq=yearly">${event['startDate'].format('dddd')}s</p>`
      : `<p class="date">${event['startDate'].format('dddd, DD MMMM')}</p>`

  let prettyStartTime = event['startDate'].format('HH:mm');

  let prettyEndTime = isSameDay(event.startDate, event.endDate)
      ? dayjs(event['DTEND']).format('HH:mm')
      : dayjs(event['DTEND']).format('DD MMMM, HH:mm');

  let allDay = isAllDay(event);

  let timeElem = allDay
      ? `<p class='event-date all-day'>
       <abbr title=${event.DTSTART} class="dt-start dtstart">
        All Day
    </abbr>
        <abbr title=${event.DTEND} class="dt-end dtend hidden">
        ${prettyEndTime}
    </abbr>
        </p>
    `
      : `<p class='event-date'>
       <abbr title=${event.DTSTART} class="dt-start dtstart">
        ${prettyStartTime}
    </abbr>
        to
        <abbr title=${event.DTEND} class="dt-end dtend">
        ${prettyEndTime}
    </abbr>
        </p>
    `

  //split description on nn wherever nn is preceded by punctuation or n immediately precedes a capital letter.
  let descriptionArr = event['DESCRIPTION']
      .split(/(?<=[\.\?\:\!])n+|n(?=[A-Z])/)
      .map(para => `<p>${para}</p>`)

  let description = descriptionArr.join('\n')

  return`
<article class="h-event vevent">
  <h2><span class="p-name p-summary summary">${event.SUMMARY}</span></h2>
    ${prettyStartDate}
    ${timeElem}
    <address class='p-location location'>
      <strong class='location-title'>Location: </strong> ${event.LOCATION ? event.LOCATION : event.URL}
    </address>
  <div class="p-description description">
    ${description}
  </div>
</article>\n
`
}

function UpcomingEventsSection (events) {
  let eventArticles = events.map(event => EventArticle(event))
  return[
    `<section class="upcoming">`,
    `<h2>Upcoming</h2>`,
    ...eventArticles,
    `</section>`
  ].join('\n')
};

function RecurringEventsSection (events) {
  let eventArticles = events.map(event => EventArticle(event))
  return[
    `<section class="recurring">`,
    `<h2>Regular Events</h2>`,
    ...eventArticles,
    `</section>`
  ].join('\n')
};

function EventsPageTemplate (events) {
  let upcoming = events.filter(e => isUpcoming(e));
  let recurring = events.filter(e => isRecurring(e));
  return [
    `<!doctype html>`,
    `<html>`,
    TEMPLATE_HEAD,
    `<body>`,
    TEMPLATE_HEADER,
    `<main id='events'>`,
    RecurringEventsSection(recurring),
    UpcomingEventsSection(upcoming),
    `<main`,
    `</body>`,
    `</html>`
  ].join('\n')
}

// tlcValue => eventValue
// all techlearningcollective events have a different time zone, one that I can't get dayjs to parse
// this formats the strings to make them a dayjs-happy format.
function convertTLCTime (timestamp) {
  let timeArr = timestamp.split('T')
  let [date, time] = timeArr
  let formattedDate = [date.substring(0,4), date.substring(4,6), date.substring(6,8)].join('-')
  let formattedTime = [time.substring(0,2), time.substring(2,4), time.substring(4,6)].join(':')
  let newTimestamp = `${formattedDate}T${formattedTime}Z`
  return newTimestamp;
}

//LocalEvent -> Event
// Turn an event object with timezone locale to one with just a DTSTART and DTEND
function consistentTimes (event) {
  let keys = Object.keys(event);
  let ctEvent;
  if (keys.includes('DTSTART')
      && keys.includes('DTEND')) {
    ctEvent = mapValues(event, (v,k) => (
      (k === 'DTSTART' || k === 'DTEND')
        ? convertTLCTime(v)
        : v))
  } else {
    let DTSTART = keys.find(key => key.startsWith('DTSTART'));
    let DTEND = keys.find(key => key.startsWith('DTEND'));
    ctEvent = {...event, DTSTART: event[DTSTART], DTEND: event[DTEND]};
  }
  return {
    ...ctEvent,
    startDate: dayjs(ctEvent['DTSTART']),
    endDate: dayjs(ctEvent['DTEND']),
  };
};

// Event -> Event
// Ensure there's a DESCRIPTION key, for events with key like DESCRIPTION;lang=en;utf-8
function consistentDescriptions (event) {
  let DESCRIPTION = Object.keys(event)
      .find(key => key.startsWith('DESCRIPTION'));
  return {
    ...event,
    DESCRIPTION: event[DESCRIPTION] ? event[DESCRIPTION].replace(/\\/g,'') : ''
  }
};

function consistentLocation (event) {
  let LOCATION = Object.keys(event)
      .find(k => k.startsWith('LOCATION'));
  return {
    ...event,
    LOCATION: event[LOCATION] ? event[LOCATION] : ''
  }
};

// CalendarData -> EventsPage
// create EventsPage using upcoming events from given CalendarData
function generateEventsPage  (cd) {
  let events = cd['VCALENDAR'][0]['VEVENT']
      .map(event => consistentTimes(event))
      .map(event => consistentDescriptions(event))
      .map(event => consistentLocation(event))
      .filter(event => isUpcoming(event) || isRecurring(event))
      .sort((a,b) => a['DTSTART'] - b['DTSTART'])

  fs.writeFile(EVENTS_PAGE_PATH, EventsPageTemplate(events), (err, data) => {
    if (err) throw new Error(err);
    console.log(`index.html written to ${EVENTS_PAGE_PATH}`)
  })
};


// Let's DO THIS.
fetch(CALENDAR_URL)
  .then(res => res.text())
  .then(text => iCal2JSON.convert(text))
  .then(calData => generateEventsPage(calData))





