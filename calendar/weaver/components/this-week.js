const dayjs = require('dayjs')
const weekOfYear = require('dayjs/plugin/weekOfYear')
const html = require('nanohtml')

const listEvents = require('./list-events.js')

dayjs.extend(weekOfYear)

module.exports = (events) => {
  var thisWeeksEvents = gatherThisWeeksEvents(events)
  if (thisWeeksEvents.length > 0) {
    return html`
    <div id='this-week'>
      <h2>ThisWeek!</h2>
      <ul>${listEvents(thisWeeksEvents)} </ul>
    </div>
      `
  } else {
    return `<!-- No Events for This Week! -->`
  }
}

function gatherThisWeeksEvents (events) {
  var today = dayjs().format('YYYY-MM-DD')
  var thisYear = dayjs().year()
  var thisWeek = dayjs().week()
  var thisWeeksEvents = events.filter(event => {
    var eventYear = dayjs(event.DATE.date).year()
    var eventWeek = dayjs(event.DATE.date).week()
    var isToday = event.DATE.date === today
    if (!isToday) {
      return eventYear === thisYear && eventWeek === thisWeek
    }
  })
  return thisWeeksEvents
}
