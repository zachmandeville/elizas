const html = require('nanohtml')
const raw = require('nanohtml/raw')

module.exports = (events) => {
  return events.map(event => {
      console.log(event)
    return html`
<li>
<div class="h-event vevent">
<h3><span class="p-name p-summary summary">${event.SUMMARY}</span></h3>
<p class='event-date'>
  <abbr title=${event.DATE.date} class="dt-start dtstart">
    ${event.DATE.dayOfWeek}, ${event.DATE.month.name} ${event.DATE.day}th at ${event.DATE.time}
  </abbr>
</p>
<p class='p-location location'><strong class='location-title'>Location: </strong> ${event.LOCATION}</p>
<div class="p-description description">
<p>${event.DESCRIPTION.replace(/\\/g,'')}</p>
<a href="${event.RAW.UID}.ics" title="ics file for ${event.SUMMARY}">Download to your own calendar!</a>
</div>
</li>
    `
  })
}
