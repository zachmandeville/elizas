const dayjs = require('dayjs')
const html = require('nanohtml')

const listEvents = require('./list-events.js')

module.exports = (events) => {
  var nextMonthsEvents = gatherNextMonthsEvents(events)
  if (nextMonthsEvents.length > 0) {
    return html`
    <div id='next-month'>
      <h2>Next Month!</h2>
      <ul>${listEvents(nextMonthsEvents)} </ul>
    </div>
      `
  } else {
    return `<!-- No Events for Next Month! -->`
  }
}

function gatherNextMonthsEvents (events) {
  var thisYear = dayjs().year()
  var nextMonth = dayjs().add(1, 'month').month()
  var thisMonth = dayjs().month()
  var nextMonthsEvents = events.filter(event => {
    var eventYear = dayjs(event.DATE.date).year()
    var eventMonth = dayjs(event.DATE.date).month()
    // console.log({event: event.SUMMARY, thisMonth, eventMonth, nextMonth})
    return eventYear === thisYear && eventMonth === nextMonth
  })
  return nextMonthsEvents
}
