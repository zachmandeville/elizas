const dayjs = require('dayjs')
const weekOfYear = require('dayjs/plugin/weekOfYear')
const html = require('nanohtml')

const listEvents = require('./list-events.js')

dayjs.extend(weekOfYear)

module.exports = (events) => {
  var thisMonthsEvents = gatherThisMonthsEvents(events)
  if (thisMonthsEvents.length > 0) {
    return html`
    <div id='this-month'>
      <h2>This Month!</h2>
      <ul>${listEvents(thisMonthsEvents)} </ul>
    </div>
      `
  } else {
    return `<!-- No Events for This Month! -->`
  }
}

function gatherThisMonthsEvents (events) {
  var today = dayjs().format('YYYY-MM-DD')
  var thisYear = dayjs().year()
  var thisWeek = dayjs().week()
  var thisMonth = dayjs().month()
  var thisMonthsEvents = events.filter(event => {
    var eventYear = dayjs(event.DATE.date).year()
    var eventWeek = dayjs(event.DATE.date).week()
    var eventMonth = dayjs(event.DATE.date).month()
    return eventYear === thisYear && eventMonth === thisMonth && eventWeek !== thisWeek
  })
  return thisMonthsEvents
}
