const dayjs = require('dayjs')
const html = require('nanohtml')

const listEvents = require('./list-events')

module.exports = (events) => {
  var todaysEvents = gatherTodaysEvents(events)
  if (todaysEvents.length > 0) {
    return html`
      <div id='today'>
        <h2>Today!</h2>
        <ul> ${listEvents(todaysEvents)} </ul>
      </div>
     `
  } else {
    return `<!-- No Events for Today! -->`
  }
}

function gatherTodaysEvents (events) {
  var today = dayjs().format('YYYY-MM-DD')
  var todaysEvents = events.filter(event => {
    return event.DATE.date === today
  })
  return todaysEvents
}
