const trimEnd = require('lodash/trimEnd')
const dayjs = require('dayjs')
const html = require('nanohtml')

const head = require('./components/head')
const breadcrumb = require('./components/breadcrumb')
const banner = require('./components/banner')
const introduction = require('./components/introduction')
const today = require('./components/today')
const thisWeek = require('./components/this-week.js')
const thisMonth = require('./components/this-month.js')
const nextMonth = require('./components/next-month.js')

function weave (events) {
 var upcomingEvents = gatherUpcomingEvents(events)
  return`
<!doctype HTML>
<html>
${head()}
<body>
${breadcrumb()}
${banner()}
${introduction()}
<div id='events'>
${today(upcomingEvents)}
${thisWeek(upcomingEvents)}
${thisMonth(upcomingEvents)}
${nextMonth(upcomingEvents)}
</div>
</body>
</html>
`
}

function gatherUpcomingEvents (events) {
  var summaries = []
  events.forEach(event => {
    if (isUpcoming(event)) {
      summaries.push({
        SUMMARY: event.SUMMARY,
        DESCRIPTION: event.DESCRIPTION,
        LOCATION: event.LOCATION,
        DATE: parseDate(event.DTSTART),
        RAW: event
      })
    }
  })
  return summaries
}

function isUpcoming (rawEvent) {
  var eventDate = parseDate(rawEvent.DTSTART)
  var event = dayjs(eventDate.date)
  var now = dayjs()
  var eventIsToday = event.format('YYYY-MM-DD') === now.format('YYYY-MM-DD')
  return eventIsToday || event.isAfter(now)
}

function parseDate (DTSTART) {
  var dateTimeAsArray = DTSTART.split('T')
  var rawDate = dateTimeAsArray[0]
  var rawTime = trimEnd(dateTimeAsArray[1], 'Z')
  var date = dayjs(rawDate).format('YYYY-MM-DD')
  var time = rawTime.replace(/..\B/g,'$&:')
  var dateAndTimes = dayjs(`${date}T${time}`)

  var formattedTime = {
    year: dateAndTimes.$y,
    month: {number: dateAndTimes.$M + 1, name: dayjs(dateAndTimes).format('MMMM')},
    day: dateAndTimes.$D,
    dayOfWeek: dateAndTimes.format('dddd'),
    time: dateAndTimes.format('HH:mm'),
    date: dateAndTimes.format('YYYY-MM-DD')
  }

  return formattedTime
}

module.exports = { weave }
