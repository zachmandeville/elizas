#!/usr/bin/env node

const axios = require('axios')
const ical2json = require('ical2json')
const fs = require('fs')
const calJson = require('./calendar.json')
const forEach = require('lodash/forEach')
const mapKeys = require('lodash/mapKeys')
const startsWith = require('lodash/startswith')
const weaver = require('./weaver')

var feedUrl = 'https://p.fruux.com/c/a3298190236/d48258b2-5588-4a7b-aef3-bc39b35c2965.ics'

var icsHeader = `
BEGIN:VCALENDAR
X-ORIGINAL-URL:https://coolguy.website
X-WR-CALNAME;CHARSET=utf-8:coolguy.website Calendar!
VERSION:2.0
METHOD:PUBLISH
BEGIN:VEVENT
`
var icsFooter = `
END:VEVENT
END:VCALENDAR
`

axios.get(feedUrl)
  .then((icsData) => updateCoolguyCalendar(icsData))
  .catch(err => console.log(`hey, you threw an error, but that's okay.  We all do it.  But the first step is going to be to fix it.  Check out axios.get: \n ${err}`))

function updateCoolguyCalendar (icsData) {
  var calData = convertFromICS(icsData.data)
  fs.writeFile('calendar.json', calData, (err) => {
    if (err) throw err
    makeIcsFiles(calJson)
    updateCalendarPage(calJson)
  })
}

function convertFromICS (icsData) {
  rawJson = ical2json.convert(icsData)
  return JSON.stringify(rawJson, null, 2)
}

function updateCalendarPage (calJson) {
  var rawEvents = calJson.VCALENDAR[0].VEVENT
  events = formatEvents(rawEvents)
  updatedPage = weaver.weave(events)
  fs.writeFile('./calendar/index.html', updatedPage, 'utf-8', (err) => {
    if (err) console.log(err)
    console.log('****\nLocal Coolguy Updated!  Sync that shit!\n****')
  })
}

function makeIcsFiles (calJson) {
  var rawEvents = calJson.VCALENDAR[0].VEVENT
  forEach(rawEvents, (event) => {
    var eventData = ical2json.revert(event)
    var eventIcs = icsHeader + eventData + icsFooter
    fs.writeFile(`./calendar/${event.UID}.ics`, eventIcs, 'utf-8', (err) => {
      if (err) console.log(err)
      console.log(`${event.SUMMARY} written!`)
    })
  })
}

function formatEvents (events) {
  formattedEvents = []
  forEach(events, (event) => {
    var newEvent = mapKeys(event, (val, key) => {
      key = makeUniform(key)
      return key
    })
    formattedEvents.push(newEvent)
  })
  return formattedEvents
}

function makeUniform (key) {
  if (startsWith(key, 'DTSTART')) {
    key = 'DTSTART'
  } else if (startsWith(key, 'DTEND')) {
    key = 'DTEND'
  }
  return key
}
