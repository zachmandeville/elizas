(ns sunrise-sunset.core
  (:gen-class)
  (:require [java-time :as time]))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(def raw-sunrise-and-sunset
  "Jan, 1,  05:51,20:57
Jan, 11, 06:01,20:56
Jan, 21, 06:12,20:51
Jan, 31, 06:25,20:43
Feb, 10, 06:38,20:32
Feb, 20, 06:50,20:18
Mar, 2,  07:02,20:03
Mar, 12, 07:14,19:47
Mar, 22, 07:25,19:30
Apr, 1,  07:36,19:14
Apr, 11, 06:46,17:57
Apr, 21, 06:57,17:42
May, 1,  07:07,17:28
May, 11, 07:18,17:16
May, 21, 07:27,17:07
May, 31, 07:36,17:01
Jun, 10, 07:42,16:58
Jun, 20, 07:47,16:58
Jun, 30, 07:48,17:01
Jul, 10, 07:46,17:07
Jul, 20, 07:40,17:15
Jul, 30, 07:31,17:24
Aug, 9,  07:20,17:33
Aug, 19, 07:07,17:43
Aug, 29, 06:52,17:53
Sep, 8,  06:35,18:03
Sep, 18, 06:18,18:13
Sep, 28, 07:01,19:23
Oct, 8,  06:44,19:34
Oct, 18, 06:28,19:45
Oct, 28, 06:14,19:57
Nov, 7,  06:01,20:09
Nov, 17, 05:51,20:21
Nov, 27, 05:44,20:33
Dec, 7,  05:41,20:43
Dec, 17, 05:42,20:51
Dec, 27, 05:47,	20:56")


(defn parse
  "Convert a CSV into rows of columns"
  [string]
  (map #(clojure.string/split % #",")
       (clojure.string/split string #"\n")))

(def sun-keys [:month :day :sunrise :sunset])

(defn str->int
  [str]
  (Integer. str))

(def conversions {:month identity
                   :day str->int
                   :sunrise identity
                   :sunset identity})

(defn convert
  [sun-key value]
  ((get conversions sun-key) (clojure.string/trim value)))

(defn mapify
  "Return a seq of maps like {:month \"jan\" :day 3 :sunrise \"06:30\" :sunset \"20:33\"})"
  [rows]
  (map (fn [unmapped-row]
         (reduce (fn [row-map [sun-key value]]
                   (assoc row-map sun-key (convert sun-key value)))
                   {}
                   (map vector sun-keys unmapped-row)))
         rows))

(def sunrise-and-sunset (mapify (parse raw-sunrise-and-sunset)))

(defn subtract-4-minutes
  [time-map]
  (let [time-obj (time/offset-time (time-map :hour) (time-map :minute))
        new-time (time/minus (time/local-time time-obj) (time/minutes 4))]
    (time/format "hh:mm"new-time)))

(defn add-4-minutes
  [time-map]
  (let [time-obj (time/offset-time (time-map :hour) (time-map :minute))
        new-time (time/plus (time/local-time time-obj) (time/minutes 4))]
    (time/format "hh:mm" new-time)))

(defn give-or-take
  "given time as string, like 06:30, return same time give or take 4 minutes.
   the give or take is random, and based on RANZ saying times would be accurate within this range."
  [time]
  (let [time-map (reduce (fn [time [k v]]
                           (assoc time k (str->int v)))
                         {}
                         (map vector [:hour :minute ] (clojure.string/split time #":")))
        dice (rand 6)]
    (cond
      (< dice 2) (subtract-4-minutes time-map)
      (and (> dice 2) (< dice 4)) (add-4-minutes time-map)
      (> dice 4) time)))

(defn get-exact-match
  [month day]
  (filter  (fn [date]
             (and (= (date :month) month) (= (date :day) day)))
           sunrise-and-sunset))

(defn get-approximate-match
  "given month and day, calculate sunrise and sunset using first set value with a higher day"
  [month day]
  (let
      [days-for-month (filter #(= (% :month) month) sunrise-and-sunset)
       next-available-date (drop-while (fn [date]
                                         (< (date :day) day)) days-for-month)
       closest-match (if (empty? next-available-date)
                       (last days-for-month)
                       (first next-available-date))]
    {:month month
     :day day
     :sunrise (give-or-take (closest-match :sunrise))
     :sunset (give-or-take(closest-match :sunset))}))

(drop-while (fn [date]
              (< (date :day) 10))
            (filter #(= (% :month) "Aug") sunrise-and-sunset))

(defn get-info-for-date
  [[month day]]
  (let
      [month month
       day (str->int day)
       exact-match (get-exact-match month day)]
    (if (not (empty? exact-match))
      exact-match
      (get-approximate-match month day))))


(get-info-for-date today)

(def tomorrow (clojure.string/split (time/format "MMM dd"(time/plus (time/local-date-time)(time/days 1))) #" "))

(def today (clojure.string/split (time/format "MMM dd"(time/local-date-time)) #" "))

((first (get-info-for-date tomorrow)) :month)
(defn pretty-print
  [sunrise-sunset-info]
  (let
      [info(first sunrise-sunset-info)]
    (println (str "           " ))
    (println (str "    "(info :month) " " (info :day)))
    (println (str "=============" ))
    (println (str "Sunrise " (info :sunrise)))
    (println (str "Sunset  " (info :sunset)))))

(defn -main
  "I don't do a whole lot ... yet."
  ([]
   (pretty-print (get-info-for-date tomorrow)))
  (
   [arg]
   (if (= arg "today")
     (pretty-print (get-info-for-date today))
     (println (get-info-for-date tomorrow)))))


