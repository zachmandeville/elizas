#+NAME: Funny Pages Eliza
#+AUTHOR: Zach Mandeville
#+DATE: 08 January 2018
#+TODO: DREAM MANIFESTING | REALITY

* The purpose
  This is a command line script to scrape all the comics I like to read each morning, so I can get them all saved and offline!
*** Wants
 - Pulls the most recent Heathcliff comic
 - Pulls the most recent Nancy comic
 - Pulls the most recent fbofw comic
 - Exports calvin and hobbes based on some internal logic.
 - If it's a weekday:
   - pulls hark a vagrant, based on an internal timer.
   - pulls mutant magic...somehow
 - If it's a weekend:
   - Pulls krazy kat, based on internal logic.
   - Pulls lil nemo, based on internal logic.
   - Pulls pbf, based on internal logic.
  
* Intentions
** REALITY Script checks the funny-pages directory and, if it isn't made yet, creates a folder for the current day.
   CLOSED: [2019-01-09 Wed 08:24]
** REALITY If it is already made, then it ignores it!
   CLOSED: [2019-01-09 Wed 08:24]
   
   it does this automatically
** DREAM clean up naming of scripts to be clearer when just looking at the code.
** DREAM Figure out how to add a single line of comment to the code from the org file
** REALITY Change file convention for where we write it to account for year now and not just month
   CLOSED: [2019-01-09 Wed 15:08]
** REALITY add logic for Calvin and Hark A Vagrant
   CLOSED: [2019-01-09 Wed 15:08]
** DREAM Automate this eliza running each morning.
* Funny Pages Scraper
** Concept
* Code for The Comics
  :PROPERTIES:
  :header-args: :tangle ./funniesEliza.rb
  :END:
** Overview
There are seven comics in a daily funny pages.
- Heathcliff, available on gocomics.com
- Nancy, also available on gocomics.com
- For Better or For Worse, available on fborfw.com
- Hark a Vagrant, on hark-a-vagrant.com
- Super Mutant Magic Academy, on mutantmagic.tumblr.com
- Calvin & Hobbes, saved to my computer.
- Far side, on my computer but one big book that needs screenshots.

So with these 7 comics, 5 of them are online across 4 different webpages and each has a different file-naming convention.  The other two need to be copied from my computer (and also have different file naming conventions.)

In the end, I want them all on my computer, in the right day's folder, with this type of filename:
~COMIC-NAME_PUBLICATION-DATE~, with publication date represented as ~YYYY-MM-DD~

So I will write a number of scripts that parse the unique webpages and save them in the same way.
** Imports and Setup
   We will add the shebang to be able to run this in the command line,k and bring in our big imports
   #+NAME: Imports and Setup
   #+BEGIN_SRC ruby
     #!/bin/env ruby 
     require 'date'
     require 'nokogiri' 
     require 'open-uri'
     require 'fileutils'

   #+END_SRC
   date lets us grab today and yesterday's date, nokogiri and open-uri are used for web parsing.
** Grab yesterday's day (current day in US) Ruby
All of these strips come from North America, which is a day behind me.  Whenever I want 'today's comics', I'd want to search for yesterday's comics online.  Since I'll want to reference yesterday and today often, I'll write a hash that brings it up whenever.

  Using built-in 'date' option.  %B gives full month name. We'll make a hash so we can reference this later. We'll also include today's date in this hash for when we want to write to file or anything else.
  #+NAME: grab-yesterdays-date
  #+BEGIN_SRC ruby :results value verbatim :tangle ./funniesEliza.rb
    def yesterday_hash
      date = Hash.new
      date['yr'] = Date.today.prev_day.strftime('%y')
      date['year'] = Date.today.prev_day.strftime('%Y')
      date['month'] = Date.today.prev_day.strftime('%B')
      date['mon_num'] = Date.today.prev_day.strftime('%m')
      date['day'] = Date.today.prev_day.strftime('%d')
      date['today'] = Date.today.strftime('%d')
      return date
    end

   #+END_SRC
** Heathcliff and Nancy
Heathcliff and Nancy are on gocomics, and you can grab the image easily from the twitter meta tag (it's the image that shows up whenever you share this comic on social media.  So we are essentially doing the same.)
*** Parse yesterday's url for exact image link.
    #+NAME: grab_image_link
    #+BEGIN_SRC ruby :results value :noweb yes
      def grab_image_link comic
        date = yesterday_hash
        year = date['year']
        month = date['mon_num'] 
        day = date['day'] 
        comic_page = Nokogiri::HTML(open("https://www.gocomics.com/#{comic}/#{year}/#{month}/#{day}"))
        image = comic_page.at("meta[name='twitter:image']")["content"]
        return image
      end
    #+END_SRC
    #+RESULTS: grab_image_link
*** Download heathcliff and nancy and Write to File
   #+NAME: Download gocomics
   #+BEGIN_SRC ruby :noweb yes :results output :dir ~/Documents/funny-pages/
     def write_comic_to_file comic
       date = yesterday_hash
       image = grab_image_link comic
       filename = "/Users/Nelsonian/Documents/funny-pages/#{date['year']}/#{date['month']}/#{date['today']}/#{comic}_#{date['year']}-#{date['mon_num']}-#{date['day']}.png"
       File.open(filename, 'wb') do |fo|
         fo.write open(image).read
       end 
     end
   #+END_SRC
   #+RESULTS: Download gocomics
** For better or for worse
*** Parse webpage for comic strip
  #+NAME: parse fborfw for image
  #+BEGIN_SRC ruby :results output
    def grab_fbofw_image
      year = Date.today.strftime('%y')
      month = Date.today.prev_day.strftime('%m')
      day = Date.today.prev_day.strftime('%d')
      strip_address = "https://www.fborfw.com/strip_fix/strips/fb#{year}#{month}#{day}.gif"
      return strip_address
    end
  #+END_SRC
  #+RESULTS: parse fborfw for image
     
*** Write For better or for worse to file
   #+NAME: Download fborfw
   #+BEGIN_SRC ruby :noweb yes :results output :dir ~/Documents/funny-pages/
     def write_fbofw_to_file
       date = yesterday_hash
       image = grab_fbofw_image
       filename = "/Users/Nelsonian/Documents/funny-pages/#{date['year']}/#{date['month']}/#{date['today']}/fbofw_#{date['year']}-#{date['mon_num']}-#{date['day']}.png"

       File.open(filename, 'wb') do |fo|
         fo.write open(image).read
       end 
     end
   #+END_SRC
   #+RESULTS: Download fborfw
** Calvin & Hobbes
   For C&H I'll want to start in the calvin&hobbes directory and, based on the tally of last file, go to the exact year/month/day file.  the files are titled nicely already, so I essentially need to break the filename up into its exact date, add a day to that, and then find the file that corresponds to that day.  Then, update the tally.
    
   #+NAME: grab-calvin-from-tally
   #+BEGIN_SRC ruby :dir ~/Documents/funny-pages/
     def calvin_tally
       File.open('/Users/Nelsonian/Documents/funny-pages/comics_tally.txt') do |file|
       calvin = file.find {|line| line =~ /calvin/}
       yesterday = calvin.split('=')[1]
       return yesterday
     end
    end

   #+END_SRC
   #+RESULTS: grab-calvin-from-tally
   : :calvin_tally
    
   #+NAME: determine-today
   #+BEGIN_SRC ruby :noweb yes :results value
     def determine_today
      tally = calvin_tally
      yesterday = DateTime.parse(tally).to_date
      today = (yesterday + 1).to_s
      return today
     end
   #+END_SRC
    
   #+NAME: copy-calvin-comic
   #+BEGIN_SRC ruby :noweb yes :results value
     def write_calvin_to_file
      today =  determine_today.split('-')
      year = today[0]
      month = today[1]
      day = today[2]
      date = yesterday_hash
      calvin_file = "/Users/Nelsonian/Documents/funny-pages/calvin&hobbes/#{year}/#{month}/#{year}#{month}#{day}.gif"
      funny_pages_file = "/Users/Nelsonian/Documents/funny-pages/#{date['year']}/#{date['month']}/#{date['today']}/calvin&hobbes_#{year}-#{month}-#{day}.gif"
      FileUtils.cp(calvin_file, funny_pages_file)
     end
   #+END_SRC

   #+RESULTS: copy-calvin-comic

** Hark a Vagrant
  #+NAME: determine-current-day-from-tally
  #+BEGIN_SRC ruby :dir ~/Documents/funny-pages/ :results value verbatim
    def todays_number
     File.open('/Users/Nelsonian/Documents/funny-pages/comics_tally.txt') do |file|
       hark = file.find {|line| line =~ /hark/}
       harkNumber = hark.split('=')[1]
       return harkNumber.to_i + 1
     end
    end
  #+END_SRC

  #+RESULTS: determine-current-day-from-tally
  : 70
   
  #+NAME: parse hark-a-vagrant for image
  #+BEGIN_SRC ruby :noweb yes :results value :dir ~/Documents/funny-pages
    def grab_hark_image 
      filename = "http://www.harkavagrant.com/index.php?id=#{todays_number}"
      comic_page = Nokogiri::HTML(open(filename))
      image = comic_page.at(".rss-content img")["src"]
      return image
    end
  #+END_SRC
  #+RESULTS: parse hark-a-vagrant for image
  : :grab_hark_image
   
  #+NAME: Download Hark a Vagrant
  #+BEGIN_SRC ruby :noweb yes :dir ~/Documents/funny-pages/
    def write_hark_to_file
      date = yesterday_hash
      image = grab_hark_image
      filename = "/Users/Nelsonian/Documents/funny-pages/#{date['year']}/#{date['month']}/#{date['today']}/hark-a-vagrant_#{todays_number}.png"

      File.open(filename, 'wb') do |fo| 
        fo.write open(image).read
      end
    end
  #+END_SRC
  #+RESULTS: Download Hark a Vagrant
  : :write_hark_to_file
    
** Update Tallies
  #+NAME: update calvin tally
  #+BEGIN_SRC ruby :noweb yes :results output :dir ~/Documents/funny-pages/
    def update_calvin_tally number
      text = File.read('/Users/Nelsonian/Documents/funny-pages/comics_tally.txt')
      File.write('comics_tally.txt', text.gsub(/^calvin&hobbes=([0-9]+)/,"calvin&hobbes=#{number}"))
    end
 #+END_SRC
  
 #+RESULTS: update calvin tally
  
  #+NAME: update hark tally
  #+BEGIN_SRC ruby :noweb yes :results output :dir ~/Documents/funny-pages 
     def update_hark_tally number
      text = File.read('/Users/Nelsonian/Documents/funny-pages/comics_tally.txt')
      File.write('comics_tally.txt', text.gsub(/^hark=([0-9]+)/,"hark=#{number}"))
     end
 #+END_SRC
   
** Bring it all Home
   This will runn all our functions
   #+NAME: run all the functions
   #+BEGIN_SRC ruby
     write_comic_to_file 'heathcliff'
     write_comic_to_file 'nancy'
     write_fbofw_to_file
     write_calvin_to_file
     write_hark_to_file
     print "Heathcliff, Nancy, Fbofw, Calvin & Hobbes, and 'Hark! A Vagrant' written to file\n"
     current = determine_today.gsub('-','')
     update_calvin_tally(current)
     print "calvin tally updated to #{current}\n"
     update_hark_tally(todays_number)
     print "hark tally updated to #{todays_number}\n"
   #+END_SRC
    
