#!/bin/env ruby 
require 'date'
require 'nokogiri' 
require 'open-uri'
require 'fileutils'

def yesterday_hash
  date = Hash.new
  date['yr'] = Date.today.prev_day.strftime('%y')
  date['year'] = Date.today.prev_day.strftime('%Y')
  date['month'] = Date.today.prev_day.strftime('%B')
  date['mon_num'] = Date.today.prev_day.strftime('%m')
  date['day'] = Date.today.prev_day.strftime('%d')
  date['today'] = Date.today.strftime('%d')
  return date
end

def grab_image_link comic
  date = yesterday_hash
  year = date['year']
  month = date['mon_num'] 
  day = date['day'] 
  comic_page = Nokogiri::HTML(open("https://www.gocomics.com/#{comic}/#{year}/#{month}/#{day}"))
  image = comic_page.at("meta[name='twitter:image']")["content"]
  return image
end

def write_comic_to_file comic
  date = yesterday_hash
  image = grab_image_link comic
  filename = "/Users/Nelsonian/Documents/funny-pages/#{date['year']}/#{date['month']}/#{date['today']}/#{comic}_#{date['year']}-#{date['mon_num']}-#{date['day']}.png"
  File.open(filename, 'wb') do |fo|
    fo.write open(image).read
  end 
end

def grab_fbofw_image
  year = Date.today.strftime('%y')
  month = Date.today.prev_day.strftime('%m')
  day = Date.today.prev_day.strftime('%d')
  strip_address = "https://www.fborfw.com/strip_fix/strips/fb#{year}#{month}#{day}.gif"
  return strip_address
end

def write_fbofw_to_file
  date = yesterday_hash
  image = grab_fbofw_image
  filename = "/Users/Nelsonian/Documents/funny-pages/#{date['year']}/#{date['month']}/#{date['today']}/fbofw_#{date['year']}-#{date['mon_num']}-#{date['day']}.png"

  File.open(filename, 'wb') do |fo|
    fo.write open(image).read
  end 
end

def calvin_tally
   File.open('/Users/Nelsonian/Documents/funny-pages/comics_tally.txt') do |file|
   calvin = file.find {|line| line =~ /calvin/}
   yesterday = calvin.split('=')[1]
   return yesterday
 end
end

def determine_today
 tally = calvin_tally
 yesterday = DateTime.parse(tally).to_date
 today = (yesterday + 1).to_s
 return today
end

def write_calvin_to_file
 today =  determine_today.split('-')
 year = today[0]
 month = today[1]
 day = today[2]
 date = yesterday_hash
 calvin_file = "/Users/Nelsonian/Documents/funny-pages/calvin&hobbes/#{year}/#{month}/#{year}#{month}#{day}.gif"
 funny_pages_file = "/Users/Nelsonian/Documents/funny-pages/#{date['year']}/#{date['month']}/#{date['today']}/calvin&hobbes_#{year}-#{month}-#{day}.gif"
 FileUtils.cp(calvin_file, funny_pages_file)
end

def todays_number
 File.open('/Users/Nelsonian/Documents/funny-pages/comics_tally.txt') do |file|
   hark = file.find {|line| line =~ /hark/}
   harkNumber = hark.split('=')[1]
   return harkNumber.to_i + 1
 end
end

def grab_hark_image 
  filename = "http://www.harkavagrant.com/index.php?id=#{todays_number}"
  comic_page = Nokogiri::HTML(open(filename))
  image = comic_page.at(".rss-content img")["src"]
  return image
end

def write_hark_to_file
  date = yesterday_hash
  image = grab_hark_image
  filename = "/Users/Nelsonian/Documents/funny-pages/#{date['year']}/#{date['month']}/#{date['today']}/hark-a-vagrant_#{todays_number}.png"

  File.open(filename, 'wb') do |fo| 
    fo.write open(image).read
  end
end

def update_calvin_tally number
  text = File.read('/Users/Nelsonian/Documents/funny-pages/comics_tally.txt')
  File.write('comics_tally.txt', text.gsub(/^calvin&hobbes=([0-9]+)/,"calvin&hobbes=#{number}"))
end

def update_hark_tally number
 text = File.read('/Users/Nelsonian/Documents/funny-pages/comics_tally.txt')
 File.write('comics_tally.txt', text.gsub(/^hark=([0-9]+)/,"hark=#{number}"))
end

write_comic_to_file 'heathcliff'
write_comic_to_file 'nancy'
write_fbofw_to_file
write_calvin_to_file
write_hark_to_file
print "Heathcliff, Nancy, Fbofw, Calvin & Hobbes, and 'Hark! A Vagrant' written to file\n"
current = determine_today.gsub('-','')
update_calvin_tally(current)
print "calvin tally updated to #{current}\n"
update_hark_tally(todays_number)
print "hark tally updated to #{todays_number}\n"
