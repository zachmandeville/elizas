#+NAME: Bookshelf Eliza
#+AUTHOR: Zach Mandeville
#+TODO: DREAM MANIFESTING | REALITY

* Intentions
  Keep an org file that I can export with pandoc and upon this export it creates a nice bookshelf page where it lists all books wiht a note link and upon clicking that link you get a full screen of a certain book and the notes that didn't show before. 
** REALITY The page is separate from our library, so we can code it separate from our list of books
** REALITY Make a stylesheet
   CLOSED: [2019-01-02 Wed 17:34]
fucking DONE   
** REALITY If javascript deisabled, the page shows a nice list of books with notes for each one.
   CLOSED: [2019-01-02 Wed 18:16]
   In other words, apply the hidden style once the dom content has loaded, whichmeans it won't be hidden if there is no javascript.
** REALITY with enabled, shows just the books and notes is hidden
   CLOSED: [2019-01-02 Wed 18:16]
** REALITY upon clicking the notes link, you see a big page view of the book and our notes.
   CLOSED: [2019-01-13 Sun 18:34]
** DREAM you can keep an image for each of the book selections
   There's a twinning happening in this project, where I have an org file that I am maintaining with all my notes and such about the books, and there's a web site that details these notes and books in a much different way.  
   
   So to add images to this, I have to be able to add images to my org file and ahve them export....and I need to be able to have these images be in my coolguy page automatically too.
   
*** Adding images to an org file   
    This can be done by first adding an image to =~/org/coolguy/assets= and then inserting it into the org file with =,il= and choosing =file=.  
    This will just show an underlined title for whatever you give it.  This is fine byme, but if you wanna see the images you can do =spc spc= and then choose =org-display-inline-images=.  This means you can toggle images on and off with the same command.
*** Images automatically added to coolguy 
    The simplest way I found for this was to create a symbolic link between the directories so that, in my filestystems eyes, the org/assets was the same directory as coolguy/bookshelf/assets.  Anything added to org would then be in coolguy inherently.
   
    To make the symbolink link, we are using the =lndir= command, like so.
   #+BEGIN_SRC shell
     mkdir ~/Projects/Web/coolguy/bookshelf/assets
     lndir ~/org/coolguy/bookshelf/assets ~/Projects/Web/coolguy/bookshelf/assets
     ls ~/Projects/Web/coolguy/bookshelf/assets
   #+END_SRC

   #+RESULTS:
   | introducing-introducing-wittgenstein-195031197.jpg |
   | mtmte-vol-1.jpg                                    |

** DREAM if you go to a specific book (with an anchor tag), it knows to apply the styling so the full view appears (in other words, if url is =/= it shows full bookshelf, if it is =/#book= it shows the fullscreen notes of that book.
** DREAM The page is accessible
** DREAM you can capture your notes about the current book you're reading.  So as you are reading it you use org-captuere and it throws notes into the proper section.

* Formatting the Org File
** Setting the Header file
   You can make the files standalone, which'll add all this html stuff to it, but I don't like their version of the html.  so instead, I'm just adding my own header as a file that gets added to the top.
   #+NAME: The header
   #+BEGIN_SRC html :tangle ~/org/coolguy/bookshelf/bookshelfheader.html
  <link rel='stylesheet' href='bookstyles.css' type='text/css' /> 
   #+END_SRC
** Overall Setting  
   this is what my settings for an export ends up looking like.
   #+BEGIN_EXAMPLE
   ((section-divs . t)
 (include-in-header . "bookshelfheader.html")
 (include-before-body . "bookshelfheading.html")
 (include-after-body . "bookscripts.html")
 (base-header-level . 2)
 (read . "org")
 (write . "html4")
 (output . "index.html")
 (output-dir . "/Users/Nelsonian/Projects/Web/coolguy/bookshelf/"))

   #+END_EXAMPLE

* Spirit
  :PROPERTIES:
  :header-args: :tangle ~/Projects/Web/coolguy/bookshelf/spirits.js
  :END:
** Set Global Variables
   #+NAME: Global Variables and Hellos
   #+BEGIN_SRC js
     /* Welcome to the Spirit Page!  Here's all the javascript that makes
        our shelves fancy!
      ,*/

     var noteHeaders = document.querySelectorAll('.level4 h4')
   #+END_SRC
** Fire up the Spirits upon Page Load
  We want the whole site to work if they have no javascript involved. It'll just be a somewhat plain listing of all the books plus notes about them. It won't be the prettiest, but they big info will be available.
  
  With javascript, we can add hidden notes, and individual pages for each book.  So, when the page loads, if there's javascript, we'll use it to alter all the basic styling of the page to it's new spirited style.
  #+NAME: Fire up the Spirits
  #+BEGIN_SRC js
    document.addEventListener('DOMContentLoaded', () => {
      addCloseButtons()
      hideNotes()
      listenForHeaderClicks()
      listenForCloseButtonClicks()
      leaveNiceNote()
    })
  #+END_SRC
** Add Close Buttons
   we can use [[https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML][insertAdjacentHTML]] for this.  We want to add it to inside our level4 section, so it should show up with all the p tags.  We'll add the hidden class to it by default.
   
  I think  we want to have it be a button instead of an a link cos of accessibility.  It's not going to a different part of the page, it's just going to trigger an event listener.
  #+NAME: addCloseButtons
  #+BEGIN_SRC js
    function addCloseButtons () {
      var notesSections = document.querySelectorAll('.level4')
      notesSections.forEach(section => {
        section.insertAdjacentHTML('beforeend', '<button class="hidden close-button">Go Back to Bookshelf</button>')
      })
    }
  #+END_SRC
** Hide Notes
   level4  is given to our code by the pandoc export, so all our notes sections within the bookshelf will have this class.  Which means that all the paragraphs within it will have a p class.  We can use the css selectors to hide all of them.
   #+NAME: Hide Notes
   #+BEGIN_SRC js
    function hideNotes () {
      var notes = document.querySelectorAll('.level4 p')
      notes.forEach(note => note.classList.toggle('hidden'))
    }
   #+END_SRC
** Listen for Header Clicks
   #+NAME: Listen for Header Clicks
   #+BEGIN_SRC js
     function listenForHeaderClicks () {
       noteHeaders.forEach(note => {
         note.addEventListener('click', toggleNotesPage)
       })
     }
   #+END_SRC
** Listen for Close-Button Clicks
   #+NAME: listenForClosebuttonClicks
   #+BEGIN_SRC js
     function listenForCloseButtonClicks () {
       var closeButtons = document.querySelectorAll('.close-button')
       closeButtons.forEach(closeButton => {
         closeButton.addEventListener('click', toggleNotesPage)
       })
     }
   #+END_SRC
   
** Toggle The Books Page upon click
   Whenever an element is clicked we can grab it's parent element, which also has a parent element.  Whenever a header or close button is clicked, we want to take the relevant book section and make it full page.  Since either of these are inside the 'level4' div, and we want to grab the relevant 'level3', it means we need to grab the parent Element of the header/closeButton's parentElement.  Kiiinda janky, but works!
     #+NAME: showNotesPage
     #+BEGIN_SRC js
       function toggleNotesPage (e) {
         var notesSection = e.target.parentElement
         var bookSection = notesSection.parentElement
         var notes = notesSection.querySelectorAll('p')
         var closeButton = notesSection.querySelector('.close-button')

         toggle(bookSection, 'fun')
         toggleAll(notes, 'hidden')
         toggle(closeButton, 'hidden')
       }
     #+END_SRC
** Toggle Chosen Element
   #+NAME: toggleAll
   #+BEGIN_SRC js
     function toggle (element, className) {
       element.classList.toggle(className)
     }
   #+END_SRC
** toggle All chosen elements
   #+NAME: toggleAll
   #+BEGIN_SRC js
     function toggleAll (elements, className) {
       elements.forEach(element => element.classList.toggle(className))
     }
   #+END_SRC
** Leave Nice Note
   :PROPERTIES:
   :END:
   Always nice to leave a good console log.
   #+BEGIN_SRC js
     function leaveNiceNote () {
       console.log(`
     Hello!

     Thank you for checking out the console!

     This is the seeeeeeeeecret shelf.

     These are my favorite books of all time:

         My Antonia, by Willa Cather
         At Swim-Two-Birds, by Flann O'brien
         Dog of the South, by Charles Portis
         Hardboiled Wonderland and the End of the World, by Haruki Murakami
         Bluets, by Maggie Nelson
         Complete Short Stories, by Vladimir Nabokov

     Thank you for reading!
        ,   ,
       /////|
      ///// |
     |~~~|  |
     |===|  |
     |j  |  |
     | g |  |
     |  s| /
     |===|/
     '---'
     `
     )
     }
   #+END_SRC
 :PROPERTIES:
 :header-args: :tangle ~/Projects/Web/coolguy/bookshelf/spirits.js
 :END:

* Style
  :PROPERTIES:
  :header-args: :tangle ~/Projects/Web/coolguy/bookshelf/bookstyles.css
  :END:
Now that we have the scripts setup to toggle them classes, let's add the classes so they look GOOOOOOOD
#+NAME: Bookshelf STYLE
#+BEGIN_SRC css
  @font-face {
      font-family: 'charter';
      src: url('../aesthetic/fonts/charter/charter_regular-webfont.woff') format('woff');
      font-weight: normal;
      font-style: normal;
  }

  body, html {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
  }

  body{
      background: url('assets/stars.gif');
      color: white;
      font-family: 'charter';
      font-size: 30px;
  }

  h2{
      letter-spacing: 0.1em;
      font-variant: small-caps;
  }

  .level2{
      display: grid;
      grid-template-columns: 1fr 1fr;
  }

  .level2:last-of-type {
      margin-bottom: 3em;
  }

  .level2 h2{
      grid-row: 1;
      grid-column: 1 / span 2;
  }

  .level2 > div:nth-child(even) {
      border-right: none;
  }

  .level2 > div:nth-last-child(2) {
      border-bottom: 2px dotted white;
  }

  .level2 > div:nth-last-child(1) {
      border-bottom: 2px dotted white;
      border-right: 2px dotted white !important;
  }

  /* level3 is the cells of each book, containing the cover, the title, the author
     and a link to the notes
  ,*/
  .level3 {
      border-top: 2px dotted white;
      border-left: 2px dotted white;
      border-right: 2px dotted white;
      padding: 0;
      display: grid;
      grid-template-areas: "cover"
                          "title"
                          "byline"
                          "notes";
  }

  .level3 img{
      grid-area: cover;
      position: relative;
      height: 40vh;
      margin: auto;
      margin-bottom: 0;
      display: flex;
  }

  .level3 p:first-of-type {
      margin-bottom: 0;
  }

  .level3 h3 {
      grid-area: title;
      margin: 0.5em 0.5em 0 0.5em;
      text-align: center;
  }

  .level3 p:last-of-type{
      grid-area: byline;
      margin-top: 0;
      margin-bottom: 1em;
      text-align: center;
  }

  .level3 h4 {
      margin-top: 0;
      margin-bottom: 1em;
      text-align: center;
      grid-area: notes;
  }

  .hidden {
      display: none !important;
  }

  #bookshelves {
      width: 90%;
      margin: auto;
  }

  .level4 h4{
      cursor: pointer;
      color: blue;
      text-decoration: underline;
  }

  .level4 p{
      display: none;
  }

  .fun {
      height: 100vh;
      overflow-y: scroll;
      z-index: 10000;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      display: grid;
      grid-template-columns: 1fr 2fr;
      background: black;
      color: peachpuff;
  }

  .fun.level3 {
      background: url('assets/stars.gif');  
  }

  .fun.level3 p:last-of-type {
      margin-top: 0;
  }

  .fun.level3 img{
      height: 70vh;
      margin: auto;
  }

  .fun.level3 h3{
      margin-top: 2em;
      margin-bottom: 0;
  }

  .fun .level4 {
      background: peachpuff;
      color: indigo;
      display: flex;
      flex-direction: column;
      grid-column: 2;
      grid-row: 1 / span 4;
      overflow-y: scroll;
      padding: 3em;

  }
  .fun .level4 h4 {
      text-decoration: none;
      color: gray;
      text-align: left;
  }

  .fun p {
      display: inline-block !important;
      text-align: left;
  }

  .fun button {
      color: gray;
      font-size: 0.8em;
      position: fixed;
      top: 7px;
      right: 7px;
      background: none !important;
      border: 0;
      cursor: pointer;

  }

  /* Let's Setup the Good Fonts */

#+END_SRC

